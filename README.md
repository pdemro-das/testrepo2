# Test LFS Repo
This repository exists to be a prototype of using Git LFS with Bitbucket pipelines

# Git LFS
LFS is Git solution for large file storage.  Large Git remote repositories like GitHub and BitBucket have native support for LFS.  For more information on Git LFS please see the documentation https://git-lfs.github.com/

# BitBucket piplines
There is a single BitBucket pipeline in the project, "POC Git LFS".  The flow for this pipeline is

1. Clone the repository natively.  The `lfs: true` block tells BitBucket to hydrate any LFS blobs automatically
2. Zip the LFS image file in the repository
3. Upload the zip to the BitBucket releases page for the repo

# Summary
The sample pipeline shows a very simple prototype for using Git LFS with BitBucket pipelines.  By adding `lfs: true` to the top of the pipeline, BitBucket autmatically (and natively) hydrates LFS blobs with no further action required by the pipeline developer. 

To confirmt his test test, if `lfs: true` is removed from the pipeline, the zipped file uploaded to releases will contain a text pointer rather than the actual image.

# Supporting documentation
https://bitbucket.org/blog/git-lfs-now-available-bitbucket-pipelines

https://support.atlassian.com/bitbucket-cloud/docs/deploy-build-artifacts-to-bitbucket-downloads/

https://bitbucket.org/bitbucketpipelines/example-aws-elasticbeanstalk-deploy/src/master/bitbucket-pipelines.yml

https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/